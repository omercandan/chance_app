//
//  LoginViewController.swift
//  ChanceApp
//
//  Created by Ömer Mert Candan on 1/26/19.
//  Copyright © 2019 Ömer Mert Candan. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import RealmSwift
import RSLoadingView

struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        var name: String?
        var email: String?
        init(rawResponse: Any?) {
            // Decode JSON from rawResponse into other properties here.
            if let result = rawResponse as? [String:String] {
                if let name = result["name"] {
                    self.name = name
                }
                if let email = result["email"] {
                    self.email = email
                }
            }
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, name, email"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

class LoginViewController: UIViewController {
    
    var realm: Realm?
    
    func setLoading(_ isLoading: Bool) {
        if isLoading {
            let loadingView = RSLoadingView()
            loadingView.show(on: self.view)
        } else {
            RSLoadingView.hide(from: self.view)
        }
    }
    
    func fetchProfile() -> Bool {
        if let realm = self.realm {
            let profile = realm.objects(Profile.self).first
            if let _ = profile?.name, let _ = profile?.email {
                return true
            }
        }
        return false
    }
    
    func profileRequest() {
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                print("Custom Graph Request Succeeded: \(response)")
                
                let profile = Profile()
                profile.name = response.name ?? "Unknown user"
                profile.email = response.email ?? "Unknown email"
                
                if let realm = self.realm {
                    do {
                        try realm.write {
                            realm.add(profile)
                        }
                    } catch {
                        print("Error writing profile to realm")
                    }
                }
                
            case .failed(let error):
                print("Custom Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    func loginComplete() {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainViewController") as? MainViewController {
            present(vc, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLoading(true)
        realm = try! Realm()
        title = "Login"
        
        print("Login View did Load")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.
            print("Access Token: \(accessToken)")
            if !fetchProfile() {
                profileRequest()
            }
            loginComplete()
        } else {
            let loginButton = LoginButton(readPermissions: [ .publicProfile, .email ])
            loginButton.center = view.center
            loginButton.delegate = self
            view.addSubview(loginButton)
        }
        
        setLoading(false)
    }

}

extension LoginViewController: LoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("logged out")
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        profileRequest()
        loginComplete()
    }
}
