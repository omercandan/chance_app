//
//  MainViewController.swift
//  ChanceApp
//
//  Created by Ömer Mert Candan on 1/26/19.
//  Copyright © 2019 Ömer Mert Candan. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import RealmSwift
import SocketIO

class MainViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    var manager: SocketManager?
    var socket: SocketIOClient!
    
    func setName(_ name: String) {
        nameLabel.text = name
    }
    
    private var profile: Profile?
    
    func addHandlers() {
        socket.on(clientEvent: .connect) { data, ack in
            print("socket connected")
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let manager = SocketManager(socketURL: URL(string: "https://46.101.122.145:1443")!, config: [.log(true), .compress, .selfSigned(true), .sessionDelegate(self)])
        socket = manager.defaultSocket
        
        addHandlers()
        
        socket.connect()
        
//        if let socket = socket {
//            print("connecting socket")
//
//        }
        
        title = "Chance"
        
        
        let realm = try! Realm()
        if let profile = realm.objects(Profile.self).first {
            self.profile = profile
            if let name = profile.name {
                setName(name)
            }
        }
    
    }
}

extension MainViewController: URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }

}
