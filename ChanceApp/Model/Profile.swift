//
//  Profile.swift
//  ChanceApp
//
//  Created by Ömer Mert Candan on 1/26/19.
//  Copyright © 2019 Ömer Mert Candan. All rights reserved.
//

import RealmSwift

class Profile: Object {
    @objc dynamic var name: String?
    @objc dynamic var email: String?
}

